package taskD;

public class Game {
    static int hp=25;
    int[] doors = new int[10];

    public void getDoors() {

        int artefact = 0;
        int monstr = 0;


        for (int i = 0; i < doors.length; i++) {
            int a = (int) (1 + Math.random() * 2);
            if (a == 1) {
                doors[i] = (int) (10 + Math.random() * 80);
                System.out.println("За дверима № " + (i + 1) + " знаходиться магічний артефакт, який дарує " + doors[i] + " HP");
                artefact++;
            } else if (a == 2) {
                doors[i] = (int) (10 + Math.random() * (-80));
                System.out.println("За дверима № " + (i + 1) + " знаходиться магічний монстр, який забирає " + doors[i] + " HP");
                monstr++;
            }
        }

    }

    public void getDoorsWithArtefacts() {
        System.out.println("Артефакти очікують за дверима №");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] > 0) {
                System.out.print(i + 1 + ", ");
            }
        }
        System.out.println(" ");
    }

    public void getDoorsWithMonstr() {
        System.out.println("Монстри очікують за дверима №");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] < 0) {
                System.out.print(i + 1 + ", ");
            }
        }
        System.out.println(" ");
    }

    public void getDoorsOfDeath(){
        System.out.println("Героя чекає смерть за дверима: ");
        for (int i = 0; i < doors.length; i++) {
            if(doors[i]+hp<0){
                System.out.print(i + 1 + ", ");
            }
        }
        System.out.println(" ");
    }

    public void playGame(){
        int result=hp;
        for (int i=0;i<doors.length;i++){
            result+=doors[i];
        }

        if (result<0){
            System.out.println("Герой приречений на смерть");
        }else {

            System.out.println("Для перемоги, відкривай двері в наступному порядку: ");
            for (int i=0;i<doors.length;i++){
                if (doors[i]>=0){
                    System.out.print(i+1 +", ");
                }
            }

            for (int i=0;i<doors.length;i++){
                if (doors[i]<0){
                    System.out.print(i+1 +", ");
                }
            }
            System.out.println(" ");
        }
    }


}
